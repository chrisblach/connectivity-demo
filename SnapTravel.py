import sys
import requests
import json
import xml.etree.ElementTree as ET
import psycopg2

try:
    conn = psycopg2.connect("dbname='snaptravel' user='postgres' host='localhost' password='root'")

except:
    print "I am unable to connect to the database"
cur = conn.cursor()
City = sys.argv[1]
Checkin = sys.argv[2]
Checkout = sys.argv[3]

r = requests.post('https://experimentation.getsnaptravel.com/interview/hotels', json=
{
    "city" : City,
    "checkin" : Checkin,
    "checkout" : Checkout,
    "provider" : 'snaptravel'
 }

)


j_object = json.loads(r.text)
jsonHotels = {}
for hotels in j_object['hotels']:
    jsonHotels[str(hotels['id'])] = str(hotels['price'])


rXML = requests.post('https://experimentation.getsnaptravel.com/interview/legacy_hotels', data='<?xml version="1.0" encoding="UTF-8"?><root><checkin>'+ Checkin + '</checkin><checkout>' + Checkout + '</checkout><city>'+ City + '</city><provider>snaptravel</provider></root>', headers={'Content-Type':'application/xml; charset=UTF-8'})


tree = ET.fromstring(rXML.content)
element = tree.findall('.//id')

xmlHotels = {}
id = ""
for elem in tree.iter():
        if elem.tag == "id":
            id = elem.text
        elif elem.tag == "price":
            xmlHotels[id] = elem.text


data = {}
cur.execute("CREATE TABLE hotel (data json);")
for key in jsonHotels:
    if key in xmlHotels:
        for hotels in j_object['hotels']:
            if hotels['id'] == int(key):
                data['id'] = hotels['id']
                data['hotel_name'] = hotels['hotel_name']
                data['num_reviews'] = hotels['num_reviews']
                data['address'] = hotels['address']
                data['num_stars'] = hotels['num_stars']
                data['amenities'] = hotels['amenities']
                data['image_url'] = hotels['image_url']
                data['prices'] = {'snaptravel': jsonHotels.get(key),'hotels.com': xmlHotels.get(key)}
                #print data
                dbjson = json.dumps(data)
                cur.execute("INSERT INTO hotel VALUES (%s)", (json.dumps(data),))
conn.commit()
conn.close()